*** Settings ***
Documentation     Petstore project
Library           Selenium2Library
Library           DateTime

*** Test Cases ***
US01 Display the current date - Date displayed
    [Documentation]    As a pet store user I want to see the current date displayed on the top right of the page
    [Tags]    Date
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    ${width}    ${height}=    Get Window Size
    Capture Page Screenshot    #Take a screenshot of the page and of how date is displayed
    ${vertical}=    Get Vertical Position    xpath:/html/body/div[1]/div/nav/div/span/div    #xpath for date
    ${horizontal}=    Get Horizontal Position    xpath:/html/body/div[1]/div/nav/div/span/div    #xpath for date
    Close Browser
    Should Be True    ${vertical}<47    #The vertical position of the Date, is 47.0 and it's in the second row. So it should be less of 47 in order to be in 1st row
    Should Be True    ${horizontal}>230    #Got the horizontal position of 1st the Delete button, was 239.0

US01 Display the current date - Date format
    [Documentation]    As a pet store user I want to see that the date format is DD-MM-YYYY
    [Tags]    Date
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    ${date} =    Get Text    xpath:/html/body/div[1]/div/nav/div/span/div
    ${date format} =    Get Current Date    result_format=%d-%m-%Y
    Should Be Equal    ${date}    ${date format}
    Log    Date format is: DD-MM-YYYY ${Date} and now date is ${date format}
    Capture Page Screenshot
    Close Browser

US01 Display the current date - Bakground color
    [Documentation]    As a pet store user I want to see that the background color of the banner is black
    [Tags]    Date
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    ${elem}    Get Webelement    css=.assignment-masthead
    ${bg color}    Call Method    ${elem}    value_of_css_property    background-color
    Log    Background color is ${bg color}
    Should be true    '${bg color}'=='rgba(0, 0, 0, 1)'    # Background color should be equal to the rgba value for black color
    Capture Page Screenshot
    Close Browser