*** Settings ***
Documentation     Petstore project
Library           Selenium2Library
Library           Collections
Library           JMeterLib
Library           OperatingSystem
Library           String


*** Test Cases ***
US02 View the list of pets
    [Documentation]    As a pet store user I want to see my current pets so that I can view all my pets in one page.
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    ${count} =    Get Element Count    class:pet-item
    Should Be True    ${count}>0    # And a list of pets is displayed, since the element count returns a number greater of zero
    # And I'm able to view my pets Name
    Page Should Contain Element    xpath://*[contains(concat(' ',normalize-space(@class),' '),' pet ') and contains(concat(' ',normalize-space(@class),' '),' lbl ') and contains(concat(' ',normalize-space(@class),' '),' pet-name ')]    limit=${count}
    # And I'm able to view my pets Status
    Page Should Contain Element    xpath://*[contains(concat(' ',normalize-space(@class),' '),' pet ') and contains(concat(' ',normalize-space(@class),' '),' lbl ') and contains(concat(' ',normalize-space(@class),' '),' pet-status ')]    limit=${count}
    Close Browser

US02 View the list of pets - 100
    [Documentation]    As a pet store user I want to see my current pets so that I can view all my pets in one page.
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    ${count} =    Get Element Count    class:pet-item
    Should Be True    ${count}>0    # And a list of pets is displayed, since the element count returns a number greater of zero
    : FOR    ${i}    IN RANGE    100-${count}
    \    Page Should Contain Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[1]/div/input    message=None    loglevel=INFO
    \    Page Should Contain Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[2]/div/input    message=None    loglevel=INFO
    \    Press Key    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[1]/div/input    TestName_${i}    # Give TestName as the Pet Name
    \    Press Key    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[2]/div/input    TestStatus_${i}    # Give TestStatus as the Pet Status
    \    Press Key    id:btn-create    \\13    # ASCII code for enter key
    \    Exit For Loop If    ${i} == 100
    \    Log    ${i}
    Log    Exited..
    ${count} =    Get Element Count    class:pet-item    # Get the number of pets again after adding the pets through the loop
    # And I'm able to view my pets Name
    Page Should Contain Element    xpath://*[contains(concat(' ',normalize-space(@class),' '),' pet ') and contains(concat(' ',normalize-space(@class),' '),' lbl ') and contains(concat(' ',normalize-space(@class),' '),' pet-name ')]    limit=${count}
    # And I'm able to view my pets Status
    Page Should Contain Element    xpath://*[contains(concat(' ',normalize-space(@class),' '),' pet ') and contains(concat(' ',normalize-space(@class),' '),' lbl ') and contains(concat(' ',normalize-space(@class),' '),' pet-status ')]    limit=${count}
    ${result}    run jmeter analyse jtl convert    C:/Jmeter/apache-jmeter-4.0/bin/jmeter.bat    C:/Users/pvlachop/Documents/Jmeter_TestPlans/Petstore.jmx    C:/Users/pvlachop/Documents/Jmeter_TestPlans/PetstoreOutputTable.jtl
    log    ${result}
    : FOR    ${ELEMENT}    IN    @{result}
    \    log dictionary    ${ELEMENT}
    ${FILE_CONTENT}=    Get File    C:/Users/pvlachop/Documents/Jmeter_TestPlans/PetstoreOutputTable.jtl
    Log    File Content: ${FILE_CONTENT}
    @{LINES}=    Split to Lines    ${FILE_CONTENT}
    Log    ${LINES}
    ${ret} =    Grep File    C:/Users/pvlachop/Documents/Jmeter_TestPlans/PetstoreOutputTable.jtl    %elapsed%
    Close Browser