*** Settings ***
Documentation     Petstore project
Library           Selenium2Library
Library           ImageHorizonLibrary

*** Test Cases ***
US03 Be able to add a new pet - Mandatory Fields
    [Documentation]    As a pet store user I want to be able to add a new pet so that I can add new pets to my collection
    [Tags]    Add new pet
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    Press Key    id:btn-create    \\13    # ASCII code for enter key
    Page Should Contain    Pet name and/or pet status are missing. Please try again.    # They are not mandatory fields a pet can be added with empty pet name and empty pet status
    Close Browser

US03 Be able to add a new pet - Enter Key
    [Documentation]    As a pet store user I want to be able to add a new pet so that I can add new pets to my collection
    [Tags]    Add new pet
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    Page Should Contain Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[1]/div/input    message=None    loglevel=INFO
    Page Should Contain Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[2]/div/input    message=None    loglevel=INFO
    Press Key    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[1]/div/input    TestName    # Give TestName as the Pet Name
    Press Key    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[2]/div/input    TestStatus    # Give TestStatus as the Pet Status
    Press Key    id:btn-create    \\13    # ASCII code for enter key
    Close Browser

US03 Be able to add a new pet - Create button
    [Documentation]    As a pet store user I want to be able to add a new pet so that I can add new pets to my collection
    [Tags]    Add new pet
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    Page Should Contain Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[1]/div/input    message=None    loglevel=INFO
    Page Should Contain Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[2]/div/input    message=None    loglevel=INFO
    Press Key    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[1]/div/input    Test_Name    # Give Test_Name as the Pet Name
    Press Key    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[2]/div/input    Test_Status    # Give Test_Status as the Pet Status
    Click Element    id:btn-create    # Click the Create button
    Close Browser

US03 Be able to add a new pet - Accessibility sequence - TAB
    [Documentation]    As a pet store user I want to be able to add a new pet so that I can add new pets to my collection
    [Tags]    Add new pet
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    Set Focus To Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[1]/div/input    #Set Focus to Pet Name
    Press Key    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[1]/div/input    \\09    # Press tab key
    Element Should Be Focused    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[2]/div/input    #Pet Status should be focused
    Press Key    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[2]/div/input    \\09    # Press tab key
    Element Should Be Focused    id:btn-create    # Create button should be focused
    Close Browser

US03 Be able to add a new pet - SHIFT + TAB
    [Documentation]    As a pet store user I want to be able to add a new pet so that I can add new pets to my collection
    [Tags]    Add new pet
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    Set Focus To Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[2]/div/input    #Set focus to Pet Status
    Press combination    Key.shift    Key.tab    # Press SHIFT+TAB -- imagehorizonlibrary
    Element Should Be Focused    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[1]/div/div/form/div[1]/div/input    #Pet Name should be focused
    Close Browser
