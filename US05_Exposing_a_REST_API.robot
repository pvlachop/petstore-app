*** Settings ***
Documentation     Petstore project
Library           Selenium2Library
Library           RequestsLibrary

*** Test Cases ***
US05 Exposing a REST API - GET /api/pets
    [Documentation]    As a pet store user I want to access my resource by using the following REST API
    [Tags]    REST API    GET
    Create Session    petstore    http://localhost:3000
    ${response}=    Get Request    petstore    /pets
    Should Be Equal As Strings    ${response.status_code}    200

US05 Exposing a REST API - GET /api/pets/{id} and id exists
    [Documentation]    As a pet store user I want to access my resource by using the following REST API
    [Tags]    REST API    GET
    Create Session    petstore    http://localhost:3000
    ${response}=    Get Request    petstore    /pets/1
    Should Be Equal As Strings    ${response.status_code}    200

US05 Exposing a REST API - GET /api/pets/{id} and id doesn't exist
    [Documentation]    As a pet store user I want to access my resource by using the following REST API
    [Tags]    REST API    GET
    Create Session    petstore    http://localhost:3000
    ${response}=    Get Request    petstore    /pets/700
    Should Be Equal As Strings    ${response.status_code}    404

US05 Exposing a REST API - PUT /api/pets/{id} and id exist
    [Documentation]    As a pet store user I want to access my resource by using the following REST API
    [Tags]    REST API    PUT
    Create Session    petstore    http://localhost:3000
    &{params}=    Create Dictionary    name=Minnie    status=Out of stock
    ${response}=    Put Request    petstore    /pets/1    params=${params}
    Should Be Equal As Strings    ${response.status_code}    200

US05 Exposing a REST API - PUT /api/pets/{id} and id doesn't exist
    [Documentation]    As a pet store user I want to access my resource by using the following REST API
    [Tags]    REST API    PUT
    Create Session    petstore    http://localhost:3000
    &{params}=    Create Dictionary    name=Minnie    status=Out of stock
    ${response}=    Put Request    petstore    /pets/700    params=${params}
    Should Be Equal As Strings    ${response.status_code}    404

US05 Exposing a REST API - POST /api/pets/{id} and id exist
    [Documentation]    As a pet store user I want to access my resource by using the following REST API
    [Tags]    REST API    POST
    Create Session    petstore    http://localhost:3000
    ${params}=    Create Dictionary    name=Minnie    status=Out of stock
    ${response}=    Post Request    petstore    /pets/1    params=${params}
    Should Be Equal As Strings    ${response.status_code}    200

US05 Exposing a REST API - POST /api/pets
    [Documentation]    As a pet store user I want to access my resource by using the following REST API
    [Tags]    REST API    POST
    Create Session    petstore    http://localhost:3000
    ${response}=    Post Request    petstore    /pets
    Should Be Equal As Strings    ${response.status_code}    201

US05 Exposing a REST API - DELETE /api/pets/{id} and id exists
    [Documentation]    As a pet store user I want to access my resource by using the following REST API
    [Tags]    REST API    DELETE
    Create Session    petstore    http://localhost:3000
    # After executing DELETE please update it with another valid id
    ${response}=    Delete Request    petstore    /pets/201
    Should Be Equal As Strings    ${response.status_code}    200

US05 Exposing a REST API - DELETE /api/pets/{id} and id doesn't exist
    [Documentation]    As a pet store user I want to access my resource by using the following REST API
    [Tags]    REST API    DELETE
    Create Session    petstore    http://localhost:3000
    ${response}=    Delete Request    petstore    /pets/700
    Should Be Equal As Strings    ${response.status_code}    404
