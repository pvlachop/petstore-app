*** Settings ***
Documentation     Petstore project
Library           Selenium2Library

*** Test Cases ***
US04 Be able to modify an existing pet - Pet Name
    [Documentation]    As a pet store user I want to be able to modify existing pets so that I can update their name or/and status
    [Tags]    Modify an existing pet
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    ${count} =    Get Element Count    class:pet-item
    Should Be True    ${count}>0    # And a list of pets is displayed, since the element count returns a number greater of zero
    ${existing petname}    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    1    #Select the Pet Name in the last line
    Click Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[1]    #Click the last Pet Name
    Input Text    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[1]/input    ${existing petname}_${count}    #Updating the Pet Name by adding to the existing name, '_' and the number of the row
    Close Browser

US04 Be able to modify an existing pet - Pet Status
    [Documentation]    As a pet store user I want to be able to modify existing pets so that I can update their name or/and status
    [Tags]    Modify an existing pet
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    ${count} =    Get Element Count    class:pet-item
    Should Be True    ${count}>0    # And a list of pets is displayed, since the element count returns a number greater of zero
    ${existing petstatus}    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    2    #Select the Pet Status in the last line
    Click Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[2]    #Click the last Pet Status
    Input Text    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[2]/input    ${existing petstatus}_${count}    #Updating the Pet Status by adding to the existing status, '_' and the number of the row
    Close Browser

US04 Be able to modify an existing pet - Click Esc Key - Pet Name
    [Documentation]    As a pet store user I want to be able to modify existing pets so that I can update their name or/and status
    [Tags]    Modify an existing pet
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    ${count} =    Get Element Count    class:pet-item
    Should Be True    ${count}>0    # And a list of pets is displayed, since the element count returns a number greater of zero
    ${existing petname}    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    1    #Select the Pet Name in the last line
    Click Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[1]    #Click the last Pet Name
    Input Text    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[1]/input    ${existing petname}_${count}    #Updating the Pet Name by adding to the existing name, '_' and the number of the row
    Press Key    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[1]/input    \\27    # Click ESC Key
    ${new petname}=    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    1    #Select the Pet Name in the last line
    Should Be True    '${new petname}'=='${existing petname}'    # Changes should be discarded and the new petname should be the same with the existing petname
    Close Browser

US04 Be able to modify an existing pet - Click Esc Key - Pet Status
    [Documentation]    As a pet store user I want to be able to modify existing pets so that I can update their name or/and status
    [Tags]    Modify an existing pet
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    ${count} =    Get Element Count    class:pet-item
    Should Be True    ${count}>0    # And a list of pets is displayed, since the element count returns a number greater of zero
    ${existing petstatus}    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    2    #Select the Pet Status in the last line
    Click Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[2]    #Click the last Pet Status
    Input Text    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[2]/input    ${existing petstatus}_${count}    #Updating the Pet Status by adding to the existing status, '_' and the number of the row
    Press Key    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[2]/input    \\27    # Click ESC Key
    ${new petstatus}=    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    2    #Select the Pet Status in the last line
    Should Be True    '${new petstatus}'=='${existing petstatus}'    # Changes should be discarded and the new petstatus should be the same with the existing petstatus
    Close Browser

US04 Be able to modify an existing pet - Click Enter Key - Pet Name
    [Documentation]    As a pet store user I want to be able to modify existing pets so that I can update their name or/and status
    [Tags]    Modify an existing pet
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    ${count} =    Get Element Count    class:pet-item
    Should Be True    ${count}>0    # And a list of pets is displayed, since the element count returns a number greater of zero
    ${existing petname}    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    1    #Select the Pet Name in the last line
    Click Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[1]    #Click the last Pet Name
    Input Text    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[1]/input    ${existing petname}_${count}    #Updating the Pet Name by adding to the existing name, '_' and the number of the row
    Press Key    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[1]/input    \\13    # Click Enter Key
    # The below is to avoid the StaleElementReferenceException error
    Wait Until Keyword Succeeds    2x    300ms    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    1
    ${new petname}=    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    1    #Select the Pet Name in the last line
    Should Be True    '${new petname}'=='${existing petname}_${count}'    # Changes should be saved and the new petname should be the same with the existing petname and '_' and the number of the row
    Close Browser

US04 Be able to modify an existing pet - Click Enter Key - Pet Status
    [Documentation]    As a pet store user I want to be able to modify existing pets so that I can update their name or/and status
    [Tags]    Modify an existing pet
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    ${count} =    Get Element Count    class:pet-item
    Should Be True    ${count}>0    # And a list of pets is displayed, since the element count returns a number greater of zero
    ${existing petstatus}    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    2    #Select the Pet Status in the last line
    Click Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[2]    #Click the last Pet Status
    Input Text    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[2]/input    ${existing petstatus}_${count}    #Updating the Pet Status by adding to the existing status, '_' and the number of the row
    Press Key    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[2]/input    \\13    # Click Enter Key
    # The below is to avoid the StaleElementReferenceException error
    Wait Until Keyword Succeeds    2x    300ms    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    2
    ${new petstatus}=    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    2    #Select the Pet Status in the last line
    Should Be True    '${new petstatus}'=='${existing petstatus}_${count}'    # Changes should be saved and the new petstatus should be the same with the existing petstatus and '_' and the number of the row
    Close Browser

US04 Be able to modify an existing pet - Click outside - Pet Name
    [Documentation]    As a pet store user I want to be able to modify existing pets so that I can update their name or/and status
    [Tags]    Modify an existing pet
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    ${count} =    Get Element Count    class:pet-item
    Should Be True    ${count}>0    # And a list of pets is displayed, since the element count returns a number greater of zero
    ${existing petname}    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    1    #Select the Pet Name in the last line
    Click Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[1]    #Click the last Pet Name
    Input Text    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[1]/input    ${existing petname}_${count}    #Updating the Pet Name by adding to the existing name, '_' and the number of the row
    Click Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/thead/tr/th[1]    #Click the label Name in the header of the Pet List
    # The below is to avoid the StaleElementReferenceException error
    Wait Until Keyword Succeeds    2x    300ms    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    1
    ${new petname}=    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    1    #Select the Pet Name in the last line
    Should Be True    '${new petname}'=='${existing petname}_${count}'    # Changes should be saved and the new petname should be the same with the existing petname and '_' and the number of the row
    Close Browser

US04 Be able to modify an existing pet - Click outside - Pet Status
    [Documentation]    As a pet store user I want to be able to modify existing pets so that I can update their name or/and status
    [Tags]    Modify an existing pet
    Open Browser    http://localhost:3000    googlechrome
    Set Window Size    400    600
    ${count} =    Get Element Count    class:pet-item
    Should Be True    ${count}>0    # And a list of pets is displayed, since the element count returns a number greater of zero
    ${existing petstatus}    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    2    #Select the Pet Status in the last line
    Click Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[2]    #Click the last Pet Status
    Input Text    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/tbody/tr[${count}]/td[2]/input    ${existing petstatus}_${count}    #Updating the Pet Status by adding to the existing status, '_' and the number of the row
    Click Element    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table/thead/tr/th[2]    #Click the label Status in the header of the Pet List
    # The below is to avoid the StaleElementReferenceException error
    Wait Until Keyword Succeeds    2x    300ms    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    2
    ${new petstatus}=    Get Table Cell    xpath:/html/body/div[2]/div[2]/div/div/div/div/div[2]/div/table    -1    2    #Select the Pet Status in the last line
    Should Be True    '${new petstatus}'=='${existing petstatus}_${count}'    # Changes should be saved and the new petstatus should be the same with the existing petstatus and '_' and the number of the row
    Close Browser

